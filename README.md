# Polychaete transcriptomes #
[Transcriptomes of luminous polychaetes to identify genes involved in bioluminescence and novel bioluminescence chemistries for medical and imaging applications](https://www.ncbi.nlm.nih.gov/bioproject/342906)

## Species ##
* [Harmothoe imbricata](https://www.ncbi.nlm.nih.gov/biosample/5770999) : [93820 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/harmothoe_imbricata_trinity.fasta.gz) of mean length 841bp, [~70M reads](https://www.ncbi.nlm.nih.gov/sra/SRX2321756) non-strand-specific, filtered with [Agalma/Biolite](https://bitbucket.org/caseywdunn/biolite) and assembled with Trinity
* [Tomopteris sp](https://www.ncbi.nlm.nih.gov/biosample/5771000)
* [Tomopteris sp](https://www.ncbi.nlm.nih.gov/biosample/5771001)
* [Flota flabelligera](https://www.ncbi.nlm.nih.gov/biosample/5771002)
* [Swima bombaviridis](https://www.ncbi.nlm.nih.gov/biosample/5771003)
* [Swima sp](https://www.ncbi.nlm.nih.gov/biosample/5771004)

For data from *Harmothoe* transcriptome, please cite: Francis, W.R., et al (2013) [A comparison across non-model animals suggests an optimal sequencing depth for de novo transcriptome assembly](https://www.ncbi.nlm.nih.gov/pubmed/23496952). **BMC Genomics** 14:167

## Assembled public data in the [downloads tab](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/) ##
* [Hermodice carunculata](https://www.ncbi.nlm.nih.gov/sra/SRX194586) : [331483 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/) of mean length 745bp, assembled with Trinity v2.2 non-strand-specific, normalized and trimmed, paper by [Mehr 2015](https://doi.org/10.1186/s12864-015-1565-6)

* [Osedax japnicus](https://www.ncbi.nlm.nih.gov/sra?term=DRP003757) : [57553 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/osedax_japonicus_trinity.renamed.fasta.gz), paper by [Miyamoto 2017](https://bmcevolbiol.biomedcentral.com/articles/10.1186/s12862-016-0844-4)

* [Osedax mucofloris](https://www.ncbi.nlm.nih.gov/sra/SRR3574511) : [59818 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/osedax_mucofloris_trinity.renamed.fasta.gz)
* [Osedax rubiplumus](https://www.ncbi.nlm.nih.gov/sra/SRR3574382) : [148721 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/osedax_rubiplumus_trinity.renamed.fasta.gz)
* [Siboglinum fiordicum](https://www.ncbi.nlm.nih.gov/sra/SRR3560206) : [111525 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/siboglinum_fiordicum_trinity.renamed.fasta.gz)

* [Chaetopterus sp](https://www.ncbi.nlm.nih.gov/sra/SRX755856) : [148608 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/Chaetopterus_SRR1646443_trinity.fasta.gz) of mean length 413bp, assembled with Trinity v2.2 non-strand-specific, normalized and trimmed, paper by [Lemer 2014](https://doi.org/10.1016/j.ympev.2014.10.019)
* [Phascolosoma perlucens](https://www.ncbi.nlm.nih.gov/sra/SRR1646442) : [108137 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/phascolosoma_perlucens_trinity.renamed.fasta.gz) of mean length 274bp
* [Siphonosoma cumanense](https://www.ncbi.nlm.nih.gov/sra/SRR1646441) : [124339 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/siphonosoma_cumanense_trinity.renamed.fasta.gz) of mean length 303bp
* [Phascolion cryptum](https://www.ncbi.nlm.nih.gov/sra/SRR1646440) : [170196 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/phascolion_cryptum_trinity.renamed.fasta.gz) of mean length 288bp
* [Nephasoma pellucidum](https://www.ncbi.nlm.nih.gov/sra/SRR1646439) : [198054 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/nephasoma_pellucidum_trinity.renamed.fasta.gz) of mean length 290bp
* [Aspidosiphon parvulus](https://www.ncbi.nlm.nih.gov/sra/SRR1646391) : [296972 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/aspidosiphon_parvulus_trinity.renamed.fasta.gz) of mean length 287bp
* [Antillesoma antillarum](https://www.ncbi.nlm.nih.gov/sra/SRR1646260) : [123163 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/antillesoma_antillarum_trinity.renamed.fasta.gz) of mean length 292bp

* [Several transcriptome sets](https://www.ncbi.nlm.nih.gov/bioproject/243848) were made for annelid phylogeny, by [Weigert 2014](https://academic.oup.com/mbe/article-lookup/doi/10.1093/molbev/msu080), **many species had low coverage, however some are reassembled and available below**

![weigert2014_dataset_77-1-5_percent_barplot.png](https://raw.githubusercontent.com/wrf/misc-analyses/master/figures_for_repo/weigert2014_dataset_77-1-5_percent_barplot.png)

* [Tomopteris helgolandica](https://www.ncbi.nlm.nih.gov/sra/SRR1237767) : [26837 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/tomopteris_helgolandica_trinity.renamed.fasta.gz)
* [Harmothoe extenuata](https://www.ncbi.nlm.nih.gov/sra/SRR1237766) : [75807 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/harmothoe_extenuata_trinity.renamed.fasta.gz)
* [Marphysa bellii](https://www.ncbi.nlm.nih.gov/sra?term=SAMN02725837) : [69216 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/marphysa_bellii_trinity.renamed.fasta.gz)
* [Magelona berkeleyi](https://www.ncbi.nlm.nih.gov/sra/SRR1257638) : [74976 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/magelona_berkeleyi_trinity.renamed.fasta.gz)
* [Owenia fusiformis](https://www.ncbi.nlm.nih.gov/sra/SRR1222288) : [170853 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/owenia_fusiformis_trinity.renamed.fasta.gz)
* [Phyllochaetopterus sp](https://www.ncbi.nlm.nih.gov/sra/SRR1257898) : [262231 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/phyllochaetopterus_trinity.renamed.fasta.gz)
* [Spiochaetopterus sp](https://www.ncbi.nlm.nih.gov/sra/SRR1224605) : [108869 transcripts](https://bitbucket.org/wrf/polychaete-transcriptomes/downloads/spiochaetopterus_trinity.renamed.fasta.gz)

## Other annelid resources ##
**NOTE** dozens of annelid transcriptome assemblies are [currently available on NCBI](https://www.ncbi.nlm.nih.gov/Traces/wgs/). See [my github repo](https://github.com/wrf/misc-analyses/tree/master/taxonomy_database) for links and simple analyses.

* Transcriptome of *Alvinella pompejana* [assembled EST set](http://www.lbgi.fr/Alvinella/) or [MPI/JGI set](https://biologydirect.biomedcentral.com/articles/10.1186/1745-6150-8-2#Declarations), by [Gagniere 2010](https://bmcgenomics.biomedcentral.com/articles/10.1186/1471-2164-11-634) and [Holder 2013](https://biologydirect.biomedcentral.com/articles/10.1186/1745-6150-8-2)
* Transcriptome of *Riftia pachyptila* [assembled EST set](https://genome.jgi.doe.gov/portal/RifpacandRidgeia_2/RifpacandRidgeia_2.info.html), by [Nyholm 2012](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0038267)
* Transcriptome of *Platynereis dumerilii* at [NCBI shotgun assembly GBZT01](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GBZT01)
* Genome of *Capitella teleta*, [Capca1](http://genome.jgi.doe.gov/Capca1/Capca1.home.html), by [Simakov 2013](https://www.ncbi.nlm.nih.gov/pubmed/23254933)
* Genome of the leech *Helobdella robusta*, [Helro1](http://genome.jgi.doe.gov/Helro1/Helro1.home.html), by [Simakov 2013](https://www.ncbi.nlm.nih.gov/pubmed/23254933)
* [Single-cell transcriptome sequencing project](https://www.ebi.ac.uk/arrayexpress/experiments/E-MTAB-5953/) of *Platynereis dumerilii*, by [Achim 2018](https://academic.oup.com/mbe/advance-article/doi/10.1093/molbev/msx336/4823215)
* Transcriptome of the leech *Hirudo medicinalis* at [NCBI shotgun assembly GBRF01](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GBRF01), by [Hibsh 2015](https://www.nature.com/articles/sdata201515)

## Other spiralian data resources ##
* Genome of the limpet *Lottia gigantea*, [Lotgi1](http://genome.jgi.doe.gov/Lotgi1/Lotgi1.home.html), by [Simakov 2013](https://www.ncbi.nlm.nih.gov/pubmed/23254933)
* Genome of the oyster *Crassostrea gigas*, [v9](http://gigadb.org/dataset/view/id/100030), by [Zhang 2012](https://www.nature.com/articles/nature11413)
* Genome of the golden mussel *limnoperna fortunei* [v1](http://gigadb.org/dataset/100386), by [Uliano-Silva 2018](https://academic.oup.com/gigascience/article/7/2/1/4750781)
* Several genomes of various invertebrates are available at the [OIST Marine Genomics page](http://marinegenomics.oist.jp/gallery/gallery/index), including the brachiopod *Lingula anatina* [v2](http://marinegenomics.oist.jp/lan_v2/viewer/info?project_id=50) (by [Luo 2015](https://www.ncbi.nlm.nih.gov/pubmed/26383154)), the nemertean *Notospermus geniculatus* [v2](http://marinegenomics.oist.jp/nge_v2/viewer/info?project_id=52) (by [Luo 2017](https://www.ncbi.nlm.nih.gov/pubmed/29203924)), the pearl oyster *Pinctada fucata* [v2](http://marinegenomics.oist.jp/pearl/viewer/info?project_id=36) (by [Takeuchi 2012](https://www.ncbi.nlm.nih.gov/pubmed/22315334)) and the hemichordate worm *Ptychodera flava* [v3](http://marinegenomics.oist.jp/acornworm/viewer/info?project_id=33) (by [Simakov 2015](https://www.ncbi.nlm.nih.gov/pubmed/26580012) ).
* [Transcriptome of the snail](https://www.ncbi.nlm.nih.gov/nuccore/GBZZ00000000.1) *Pomacea canaliculata*
* The AmpuBase [8 Ampullariid snail transcriptome set](https://datadryad.org//resource/doi:10.5061/dryad.117cf), by [Ip 2018](https://www.ncbi.nlm.nih.gov/pubmed/29506476)
* [Several transcriptomes of luminous cephalopods](https://bitbucket.org/wrf/squid-transcriptomes), including *Vampyroteuthis*, *Pterygioteuthis*, *Octopoteuthis*, *Dosidicus*, and *Chiroteuthis*, by [Francis 2017](https://peerj.com/articles/3633/)
* [Many transcriptome sets](https://www.ncbi.nlm.nih.gov/bioproject/280093) for spiralian phylogeny, and [the new assemblies](https://datadryad.org/resource/doi:10.5061/dryad.30k4v), by [Kocot 2017](https://www.ncbi.nlm.nih.gov/pubmed/27664188)
